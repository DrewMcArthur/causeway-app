'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify');

// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4'
];

// Gulp task to minify CSS files
gulp.task('styles', function css () {
  return gulp.src('./src/css/**/*.css')
    // Auto-prefix css styles for cross browser compatibility
    //.pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    // Minify the file
    .pipe(csso())
    // Output
    .pipe(gulp.dest('./dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function js () {
  return gulp.src('./src/js/**/*.js')
    // Minify the file
    .pipe(uglify())
    // Output
    .pipe(gulp.dest('./dist/js'))
});

// copy images to dist folder
gulp.task('images', function images () {
  return gulp.src(['./src/images/**/*.JPG'])
    .pipe(gulp.dest('./dist/images'))
});

// favicon needs to be there too
gulp.task('favicon', function favicon() {
  return gulp.src('./src/favicon.ico')
    .pipe(gulp.dest('./dist/favicon.ico'))
})

// Gulp task to minify HTML files
gulp.task('pages', function html () {
  return gulp.src(['./src/**/*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist'));
});

// Clean output directory
gulp.task('clean', () => del(['dist']));

// Gulp task to minify all files
gulp.task('default', 
  gulp.series(['clean'], 
    gulp.parallel([
      'styles',
      'scripts',
      'images',
      'favicon',
      'pages'
    ])
  )
);
