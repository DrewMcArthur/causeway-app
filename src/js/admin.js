$(document).ready(function(){
	//select the current status
	if(causewayStatus) $('.open').addClass('active');
	else $('.closed').addClass('active');

	//handle button clicking
	$('.status-btns div').on('click', function(e){
		// submit form
		var data = {};
		data.stat = $(e.target).hasClass('open');
		data.name = $('.name').val();

		// define responses first, to make sure they happen
    socket.on('update-success', function(){
			$('.status-btns div').removeClass('active');
			$(e.target).addClass('active');
			$('.more-info .admin-content').hide()
			$('div.more-info').removeClass('admin')
			$('.links > .admin').removeClass('active')
			$('.moreInfoOverlay').removeClass('moreInfoOverlay')
      $("#modifications").show();
			if(imgHeight > maxHeight){
				$('#links').addClass('inPic');
				centerDiv('#links');
			}
    });
    socket.on('update-failure', function(){
      $('.name').effect("shake", "up");
    });
		if(!$(this).hasClass('active'))
		{
			socket.emit('admin-update', data);
		}
	});
});
