$('.option').on('click', function(e) {
  $('.option').removeClass('active');
  $('.picker-option').removeClass('active');
  $(e.target).addClass('active');
  $('.picker-option.' + $(e.target).attr('data-target')).addClass('active');
})