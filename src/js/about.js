$(document).ready(function(){
	if(status) $('.open').select();
	else $('.closed').select();

	$('.update-btn').on('click', function(){
		var data = {};
		data.stat = $('.open').is(':checked');
		data.pw = $('.pw').val().hashCode();
		socket.emit('admin-update', data);
		$('input.pw').val('');
		$('more-info').hide();
	});
});
