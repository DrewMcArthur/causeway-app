var causewayStatus = false;
var socket = io();
var isMobile = false;
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

  
// when someone updates the bridge's status
socket.on('update', function(data){
	setCausewayStatus(data.stat);

	// date wizardry
	var timeObj = new Date(data.time)
	var deltaDays = new Date(Date.now()).getDate() - timeObj.getDate()
	var timestring = ""
	if (deltaDays == 0)
	{
		var hours = timeObj.getHours()
		var AMPM = hours > 12 ? " PM" : " AM"
		hours = (hours + 11) % 12 + 1
		hours = ('0' + hours).slice(-2)
		var minutes = ('0' + timeObj.getMinutes()).slice(-2)
		timestring = "at " + hours + ":" + minutes + AMPM + "."
	}
	else if (deltaDays == 1)
	{
		timestring = "yesterday."
	}
	else
	{
		timestring = deltaDays + " days ago."
	}

	// add modification line
	// TODO: don't add if time older than 1 week ... eh should be server side though
	var content = data.name + " updated to " + (causewayStatus ? "open " : "closed ")
  var newline = "<p class='modification-line'>" + content + timestring + "</p>"
  addModifiedLine(newline);
});

// when the server sends a notification that someone liked the status
socket.on('newlike', function(count){
	$('.links > .like > .count').html(count);
});

var setCausewayStatus = function(stat){
	if(typeof stat !== 'undefined') causewayStatus = stat;
	if(causewayStatus)
		$('#status').text("Open")
	else
		$('#status').text("Closed")
}

var addModifiedLine = function(newline)
{
  $('#modifications').prepend(newline);

  if ($('#modifications').children().length > 7)
  {
    $('#modifications.no-gradient').removeClass('no-gradient')
    $('#modifications').addClass('with-gradient')

    if ($('#modifications').children().length > 8)
    {
      $('#modifications > p').last().remove();
    }
  }
}

var toggleCausewayStatus = function(){
	setCausewayStatus(!causewayStatus);
}
var centerDiv = function(selector){
	//given a selector, this centers the div within its parent using left
	//var pw = $(selector).parent().width();
	var pw = $(window).outerWidth(true);
	var w = $(selector).outerWidth(true);
	var l = (pw - w) / 2;
	//$(selector).css('margin','0');
	$(selector).css('left', l + 'px');
}
var handleMoreInfoLocation = function(){
	var linksHeight = $('#links').position().top;
	var height = $(window).height();
	//if the links are more than halfway down the page, then the fun begins
	if (linksHeight > (height * 3 / 5)){
		$('#status').addClass('moreInfoOverlay');
		$('.status-content').addClass('moreInfoOverlay');
		$('#links').addClass('moreInfoOverlay');
		$('.more-info').addClass('moreInfoOverlay');
	}
}

$(document).ready(function(){
	//image sizing handling
	var maxHeight = $(window).height() - 40; //don't wanna have to scroll
	var imgWidth = $('div.status-display').width(); //from the screen width
	var imgHeight = imgWidth / 313 * 235; //get the height with a ratio

	if(imgHeight > maxHeight){ //if this is bigger than the screen
		$('div.status-display').css('margin','20px auto') //reset margin
				       .height(maxHeight + 'px') //set maxHeight
				       .width((maxHeight / 235 * 313) + 'px');
					//and then calculate width
		$('#links').addClass('inPic');
		//$('.more-info').css('width', ((maxHeight / 235 * 313) - 20 + 'px'));
		centerDiv('.more-info');
	} else { //otherwise, just set the height
		$('div.status-display').height(imgHeight + 'px');
	}
	
	centerDiv('#links');

	var pages = ['about', 'howto', 'admin']
	pages.forEach(function(page) {
		$('.more-info .' + page + '-content').load(page + '.html')
	})

	//bottom button handling
	$('#links div').on('click',function(e){
    if ($(e.target).hasClass('like')) {
			socket.emit('like');
      return;
    }
    if ($(e.target).hasClass('count')) {
      return;
    }

		//link of the button we clicked
		var clickeduri = $(this).attr('data-uri');
		//where to put the information
		var wrapper = 'div.more-info';

    if (clickeduri == "admin"){
      alert("admin currently disabled due to bugs :( email me to change it: causeway@mcarthur.in");
      return;
    }

		//checking if the page we clicked is already there
		if($(wrapper).hasClass(clickeduri)){
			//if it is, then remove it, TODO: hide instead
			$(wrapper).children().hide();
			$(wrapper).removeClass(clickeduri);
			$(this).removeClass('active');
			$('.moreInfoOverlay').removeClass('moreInfoOverlay');
      $("#modifications").show();
			if(imgHeight > maxHeight){
				$('#links').addClass('inPic');
				centerDiv('#links');
			}
		}else{ 
			handleMoreInfoLocation();
			if(isMobile)
				$('.more-info').width($('.status-display').width() + 'px');
			else
				$('.more-info').width($('.status-display').width() - 60 + 'px');
      centerDiv('.more-info');

			//otherwise, load the page TODO: show instead
			$(wrapper + " > div").hide()
			$(wrapper + ' .' + clickeduri + '-content').show();
			$('#links div').removeClass('active');
			$(this).addClass('active');
			$('#links').removeClass('inPic');
			centerDiv('#links');
      $("#modifications").hide();
			$(wrapper).removeClass('howto')
				  .removeClass('admin')
				  .removeClass('about')
				  .addClass(clickeduri);
		}
	});

	// hide the loading mask now that we've set up the site
	$('.loading').fadeOut('slow', function(){ 
		$(this).remove();
	});
});
