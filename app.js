var express = require('express');
var app = express();
var fs = require('fs');
var Filter = require('bad-words');
var filter = new Filter();
var util = require('util')

// default to http
var isHTTPS = true
var http = require('http').createServer(app);

// check for certs, and enable https if they exist.
if (isHTTPS)
{
  var key = fs.readFileSync( '/etc/certs/auth-lstn.key' );
  var cert = fs.readFileSync( '/etc/certs/auth-lstn.cer' );
  http = require('https').createServer({key, cert}, app);
  isHTTPS = true;
}

io = require('socket.io')(http)

// this hosts the files located in the ./src directory
app.use(express.static(__dirname + '/dist/'));

// app.get('/howto',function(req, res)
// {
// // 	res.send(__dirname + '/src/howto.html');
// });

// the number of 'thumbs up's since the last status change
var likes = 179;
var updates = [{
                'name': "drew", 
                'time': new Date("Thu Aug 23 2018 17:15:59 GMT-0400 (EDT)"), 
                'stat': true
              }];

io.on('connection',function(socket)
{
  console.log("CONNECTION MADE: " + util.inspect(socket))
	// send the 6 most recent updates on connection
  for (var i = 6; i > 0; i--) {
    if (updates.length >= i) {
      socket.emit('update', updates[updates.length-i]);
    } 
  } 
  if (likes > 0)
  {
    socket.emit('newlike', likes);
  }

	// when an update is received, add it to our list and 
	socket.on('admin-update',function(data){
    console.log("ADMINUPDATE: " + util.inspect(data));
    var name = filter.clean(data.name);
    if (data.name == '' || name != data.name){
      socket.emit('update-failure');
      console.log("bad filtered name" + name);
    } else {
      // *don't* reset likes count and add a status update to local cache
      // likes = 0;
      updates.push({'name': name, 'time': Date.now(), 'stat': data.stat, 'likes': likes})

      //tell everyone else that is connected
      console.log("broadcasting update: " + util.inspect(updates[updates.length-1]))
      io.emit('update', updates[updates.length-1]);
      socket.emit('update-success');
    }
	});
  socket.on('like', function(){
    likes++;
    io.emit('newlike', likes);
  });
  socket.on('error', function(errObj){
    console.error("Recieved error from client: " + util.inspect(socket))
    console.error(util.inspect(errObj))
  });
});

var port = isHTTPS ? 443 : 80;
http.listen(port, function(){ //listen for requests at ipaddress:port
	console.log("Server is running on port " + port);
});
